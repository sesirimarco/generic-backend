# Install
`npm install`

# Run app
`npm start`

# DB

## Connection
localhost:27017

## Initialize
`npm run db-init`
