const service = require('./services');
const dbConn = require('./db-conn');
const bcrypt = require('bcrypt');
//const test = require('assert');

module.exports = {
  async signup(req, res) {
    try {


      let password = bcrypt.hashSync(req.body.password, 8);
      let email = req.body.email;

      const db = await dbConn.open();
      const users = db.collection('users');

      await users.find({ email: email }).toArray(function (err, items) {

        
        if (items.length == 0) {
          
          users.insertOne({
            email: email,
            password: password
          }, function (err, result) {
            if (err) {
              dbConn.close();
              res.status(500).send({ message: 'Error in signup:'+ err });
            } else {
              console.log(result.ops[0]);
              let user = result.ops[0];
              dbConn.close();

              return res.status(201).send({ token: service.createToken(user), user: user });
            }
          });

        } else {
          dbConn.close();
          res.status(500).send({ message: 'User already exist.' });
        }
        //dbConn.close();
      });

     

    }
    catch (error) {
      dbConn.close();
      res.status(500).send(error);
    }
  },
  async signin(req, res) {
    try {


      let password = req.body.password;
      let email = req.body.email;

      const db = await dbConn.open();
      const users = db.collection('users');

      await users.findOne({ email: email }, (err, user) => {
     
        if (err) { return res.status(500).send({ message: err }); }
        if (!user) {
          return res.status(404).send({ auth: false, message: 'User not exist.' });
        }

        let isPasswordValid = bcrypt.compareSync(password, user.password);
        if (!isPasswordValid) {
          return res.status(400).send({ auth: false, message: 'Usuario o password incorrecto!' });
        }
        req.user = user;
        res.status(200).send({
          message: 'success',
          auth: true,
          user: user,
          token: service.createToken(user),

        });
      });

      dbConn.close();

    }
    catch (error) {
      dbConn.close();
      res.status(500).send(error);
    }
  },
  islogged(req, res) {
    res.status(200).send({ auth: true, message: 'success' });
  },
  logout(req, resp) {
    resp.status(200).send({
      message: 'logout done.',
      auth: false

    });
  }
};