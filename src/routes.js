const express = require('express');

const controller = require('./controller');
const userCtrl = require('./controller-user');
const auth = require('./middlewares/auth');

const router = express.Router();

router.get('/something', controller.getSomething);

router.post('/', (req, res) => {
  console.log(req.body);
  res.status(200).send({message: '...'});
});

//USERS
router.post('/users/signup',(req, res)=>{
  userCtrl.signup(req, res);
}); 
router.post('/users/signin',(req, res)=>{
  userCtrl.signin(req, res);
}); 
router.get('/users/islogged',auth,(req, res)=>{
  userCtrl.islogged(req, res);
  
}); 
router.get('/users/logout',(req, res)=>{
  userCtrl.logout(req, res);
  
}); 

module.exports = router;