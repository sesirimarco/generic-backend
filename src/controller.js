const dbConn = require('./db-conn');

module.exports = {
  async getSomething(req, res) {
    try {
      // const db = await dbConn.open();
      const something = {
        message: 'something'
      };
      // dbConn.close();
      res.status(200).send(something);
    }
    catch (error) {
      res.status(500).send(error);
    }
  }
};