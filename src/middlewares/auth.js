const service = require('../services');

const isAuth = (req, resp, next) => {

  if (!req.headers.authorization) {
    return resp.status(403).send({ message: 'Do not have autorization' });
  }

  const token = req.headers.authorization.split(' ')[1];

  service.decodeToken(token)
    .then(response => {
      req.user = response;
      next();
    })
    .catch(response => {
      return resp.status(response.status).send(response);
    });
};

module.exports = isAuth;